# Thesaurus
* [Subject](#subject)
* [Covered topics](#Topics)

# Subject   :pushpin:
The goal is to create an interactive English dictionary.

# Topics covered
- manipulation of basic data types
- processing of user input
- exploitation of json data
- module importation
- similarity ratio between two words
- SQL

## Libraries :books:
- json
- os
- difflib
- mysql-connector-python