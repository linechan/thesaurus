
import mysql.connector

if __name__ == '__main__':
    # Create a connector for the database
    connector = mysql.connector.connect(
        user = "ardit700_student",
        password = "ardit700_student",
        host = "108.167.140.122",
        database = "ardit700_pm1database "
    )

    # Create a cursor to navigate through the database
    cursor = connector.cursor()

    # Get the user input
    user_input = input("Enter a word: ")
    # Create a query
    query = cursor.execute("SELECT * FROM Dictionary WHERE EXPRESSION = '%s'" % user_input)

    # Store the result : list of tuples
    results = cursor.fetchall()

    if results:
        for result in results:
            print(result[1])
    else:
        print("Word not found")