import json
import os
from difflib import get_close_matches

def suggest(user_input, data):
    suggestion = get_close_matches(user_input, data.keys())[0]

    print(suggestion)
    if len(suggestion) > 0:
        valid_suggestion = input("Did you mean \"{suggestion}\"? Enter Y (yes) or N (no): ")
        if valid_suggestion == "Y":
            return data[suggestion]
        elif valid_suggestion == "N":
            return run()
        else:
            return "Not a valid entry"
    else:
        return "Word not found"

def run():
    data = json.load(open("data.json"))
    user_input = input("Enter a word: ")

    if user_input in data:
        return data[user_input]
    elif user_input.lower() in data:
        return data[user_input.lower()]
    # Check for proper nouns in the list
    elif user_input.title() in data:
        return data[user_input.title()]
    # Try to find a matching word in the dictionary with difflib if it doesn't exist
    else:
        return suggest(user_input, data)

if __name__ == '__main__':
    if not os.path.exists("data.json"):
        print("File does not exist")
    else:
        output = run()
        if isinstance(output, list):
            print(" ".join(output))
        else:
            print(output)